package com.modyo.demo.jpr.controllers;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.client.WebClient;

import com.modyo.demo.jpr.model.domain.Ability;
import com.modyo.demo.jpr.model.domain.Pokemon;
import com.modyo.demo.jpr.model.domain.ResponseListPokemon;
import com.modyo.demo.jpr.model.domain.Sprites;
import com.modyo.demo.jpr.model.domain.Type;


@RestController
@RequestMapping(value = {"/modyo/poke-api/v1/"})
@CrossOrigin(origins = {"*","/*","*/*"}, methods = {RequestMethod.GET, RequestMethod.OPTIONS})
public class PokeModyoAPIController {
	
	@Autowired
	private WebClient webClient;	
	
	
	@GetMapping("pokemons/{page}")
	@Cacheable(value="cachePokemons")
	public ResponseEntity<List<Pokemon>> getPokemons(@PathVariable("page") Integer page) {
		int offset = 0;
		if(page <= 0)
			offset = 0;
		else
			offset = (page-1)*100;
		
		List<Pokemon> pokemons = new ArrayList<>();				
		//Call main API
		/*
		ResponseListPokemon response = webClientBuilder
										.build()
										.get()
										.uri("https://pokeapi.co/api/v2/pokemon?limit=100&offset="+String.valueOf(offset))
										.accept(MediaType.APPLICATION_JSON)
										.retrieve()
										.bodyToMono(ResponseListPokemon.class)
										.block();										
		
		if(response != null && response.getResults() != null && !response.getResults().isEmpty()) {
			return new ResponseEntity<>( response.getResults(), HttpStatus.OK);
		} else
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		*/
		String response = webClient
						.get()
						.uri("https://pokeapi.co/api/v2/pokemon?limit=100&offset="+String.valueOf(offset))
						.accept(MediaType.APPLICATION_JSON)
						.retrieve()
						.bodyToMono(String.class)
						.block();
		
		if(response != null && !response.isEmpty()) {
			JSONParser parser = new JSONParser();
			try {
			JSONObject json = (JSONObject) parser.parse(response);
			JSONArray pokemonesArray = (JSONArray) json.get("results");
			
			Iterator<JSONObject> iJsonObj = pokemonesArray.iterator();
			while(iJsonObj.hasNext()) {
				JSONObject obj = (JSONObject)iJsonObj.next();
				pokemons.add(new Pokemon((String)obj.get("name"),(String)obj.get("url")));
			}		
			
			//pokemons.stream().forEach(System.out::println);
			
			pokemons = pokemons.stream().map(x -> {
				
				Pokemon y = new Pokemon();				
				y.setName(x.getName());
				y.setUrl(x.getUrl());
			 
				String resp = webClient
						.get()
						.uri(x.getUrl())
						.accept(MediaType.APPLICATION_JSON)
						.retrieve()
						.bodyToMono(String.class)
						.block();						
						
				if(resp != null && !resp.isEmpty()) {
					JSONParser par = new JSONParser();
					try {
						JSONObject jsonObj = (JSONObject) par.parse(resp);
						//Retrieve id
						if(jsonObj.get("id") != null)
							y.setId((Long)jsonObj.get("id"));
						//Retrieve weight
						if(jsonObj.get("weight") != null)
							y.setWeight(String.valueOf((Long)jsonObj.get("weight")));
						
						//Retrieve abilities
						JSONArray jsonArray = (JSONArray) jsonObj.get("abilities");						
						if(jsonArray != null) {							
							List<Ability> abilities = new ArrayList<>();
							Iterator<JSONObject> iterator = jsonArray.iterator();
							while(iterator.hasNext()) {
								Ability a = new Ability();
								JSONObject obj = iterator.next();
								JSONObject jsonAbility = (JSONObject)obj.get("ability");								
								a.setName((String)jsonAbility.get("name"));
								a.setUrl((String)jsonAbility.get("url"));
								
								abilities.add(a);
							}		
							y.setAbilities(abilities);							
						}
						
						//Retrieve types
						jsonArray = (JSONArray) jsonObj.get("types");						
						if(jsonArray != null) {							
							List<Type> types = new ArrayList<>();
							Iterator<JSONObject> iterator = jsonArray.iterator();
							while(iterator.hasNext()) {
								Type t = new Type();
								JSONObject obj = iterator.next();
								JSONObject jsonType = (JSONObject)obj.get("type");								
								t.setName((String)jsonType.get("name"));
								t.setUrl((String)jsonType.get("url"));
								
								types.add(t);
							}		
							y.setTypes(types);					
						}
						
						//Retrieve Sprites
						JSONObject jsonSprites = (JSONObject) jsonObj.get("sprites");						
						if(jsonSprites != null) {	
							
							Sprites sp = new Sprites();	
							sp.setBack_default((String)jsonSprites.get("back_default"));								
							sp.setBack_female((String)jsonSprites.get("back_female"));
							sp.setBack_shiny((String)jsonSprites.get("back_shiny"));
							sp.setBack_shiny_female((String)jsonSprites.get("back_shiny_female"));
							sp.setFront_default((String)jsonSprites.get("front_default"));
							sp.setFront_female((String)jsonSprites.get("front_female"));
							sp.setFront_shiny((String)jsonSprites.get("front_shiny"));
							sp.setFront_shiny_female((String)jsonSprites.get("front_shiny_female"));
							if(jsonSprites.get("other") != null) {
								JSONObject jsonOther = (JSONObject) jsonSprites.get("other");
								if(jsonOther.get("dream_world") != null) {
									JSONObject jsonDW = (JSONObject) jsonOther.get("dream_world");	
									sp.setOther_dw_front_default((String)jsonDW.get("front_default"));
									sp.setOther_dw_front_female((String)jsonDW.get("front_female"));
								}
								if(jsonOther.get("official-artwork") != null) {
									JSONObject jsonOA = (JSONObject) jsonOther.get("official-artwork");
									sp.setOther_official_front_default((String)jsonOA.get("front_default"));
								}
							}
							y.setSprites(sp);					
						}
						
					} catch(Exception ex) {
						ex.printStackTrace();						
					}
				} 
				return y;
				
			}).collect(Collectors.toList());
			
			} catch(Exception ex) {
				ex.printStackTrace();
			}
			
			
			return new ResponseEntity<>(pokemons , HttpStatus.OK);
		} else
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		
	}	
	
	@GetMapping("pokemon/{name}")
	public Pokemon getPokemon(@PathVariable("name") String name) {		
		
		return webClient
					.get()
					.uri("https://pokeapi.co/api/v2/pokemon/"+name)
					.retrieve()
					.bodyToMono(Pokemon.class).
					block();		
		
	}	
	
}
