package com.modyo.demo.jpr.model.domain;

import java.io.Serializable;

public class PokemonAbility implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5408878412448761336L;
	private Boolean is_hidden;
	private Integer slot;
	private Ability ability;
	
	public PokemonAbility() {
		super();		
	}

	public PokemonAbility(Boolean is_hidden, Integer slot, Ability ability) {
		super();
		this.is_hidden = is_hidden;
		this.slot = slot;
		this.ability = ability;
	}

	public Boolean getIs_hidden() {
		return is_hidden;
	}

	public void setIs_hidden(Boolean is_hidden) {
		this.is_hidden = is_hidden;
	}

	public Integer getSlot() {
		return slot;
	}

	public void setSlot(Integer slot) {
		this.slot = slot;
	}

	public Ability getAbility() {
		return ability;
	}

	public void setAbility(Ability ability) {
		this.ability = ability;
	}	
}
