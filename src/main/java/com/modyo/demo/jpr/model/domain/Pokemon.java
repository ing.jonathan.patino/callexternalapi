package com.modyo.demo.jpr.model.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Pokemon implements Serializable {	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8929164025910041695L;
	private Long id;
	private String name;
	private String url;
	private String weight;	
	private String description;
	//private List<PokemonType> types;
	//private List<PokemonAbility> abilities;
	private List<Type> types;
	private List<Ability> abilities;
	private Sprites sprites;
	
	public Pokemon() {
		super();		
	}	

	public Pokemon(String name, String url) {
		super();
		this.name = name;
		this.url = url;
	}



	public Pokemon(Long id, String name, String weight) {
		super();
		this.id = id;
		this.name = name;
		this.weight = weight;
		this.types = new ArrayList<>();
	}
	
	/*
	public Pokemon(Integer id, String name, List<PokemonType> types, String weight) {
		super();
		this.id = id;
		this.name = name;
		this.types = types;
		this.weight = weight;
	}*/
	
	public Pokemon(Long id, String name, List<Type> types, String weight) {
		super();
		this.id = id;
		this.name = name;
		this.types = types;
		this.weight = weight;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	/*
	public List<PokemonType> getTypes() {
		return types;
	}
	public void setTypes(List<PokemonType> types) {
		this.types = types;
	}
	*/
	
	
	public String getWeight() {
		return weight;
	}
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<Type> getTypes() {
		return types;
	}

	public void setTypes(List<Type> types) {
		this.types = types;
	}

	public List<Ability> getAbilities() {
		return abilities;
	}

	public void setAbilities(List<Ability> abilities) {
		this.abilities = abilities;
	}

	public void setWeight(String weight) {
		this.weight = weight;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}		

	public Sprites getSprites() {
		return sprites;
	}

	public void setSprites(Sprites sprites) {
		this.sprites = sprites;
	}
	
	@Override
	public String toString() {
		
		return this.name+"->"+this.url;
	}
}
