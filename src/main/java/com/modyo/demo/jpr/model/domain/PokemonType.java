package com.modyo.demo.jpr.model.domain;

import java.io.Serializable;

public class PokemonType implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8778941265772645500L;
	private Integer slot;
	private Type type;
	
	public PokemonType() {
		super();
	}

	public PokemonType(Integer slot, Type type) {
		super();
		this.slot = slot;
		this.type = type;
	}

	public Integer getSlot() {
		return slot;
	}

	public void setSlot(Integer slot) {
		this.slot = slot;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

}
