package com.modyo.demo.jpr.model.domain;

import java.io.Serializable;
import java.util.List;

public class ResponseListPokemon implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5196867178312153793L;
	
	private Integer count;
	private String next;
	private String previous;
	private List<Pokemon> results;
	
	public ResponseListPokemon() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public String getNext() {
		return next;
	}

	public void setNext(String next) {
		this.next = next;
	}

	public String getPrevious() {
		return previous;
	}

	public void setPrevious(String previous) {
		this.previous = previous;
	}

	public List<Pokemon> getResults() {
		return results;
	}

	public void setResults(List<Pokemon> results) {
		this.results = results;
	}		
}
