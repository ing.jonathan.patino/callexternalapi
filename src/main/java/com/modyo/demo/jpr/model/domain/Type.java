package com.modyo.demo.jpr.model.domain;

import java.io.Serializable;

public class Type implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5893705689458448438L;
	private String name;
	private String url;
	
	
	public Type() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Type(String name, String url) {
		super();
		this.name = name;
		this.url = url;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}

}
