package com.modyo.demo.jpr.model.domain;

import java.io.Serializable;

public class Ability implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4843411857333324504L;
	private String name;
	private String url;
	
	public Ability() {
		super();		
	}	

	public Ability(String name, String url) {
		super();
		this.name = name;
		this.url = url;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

}
