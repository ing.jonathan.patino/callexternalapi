package com.modyo.demo.jpr;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

import com.modyo.demo.jpr.controllers.PokeModyoAPIController;
import com.modyo.demo.jpr.model.domain.Pokemon;

@SpringBootTest
class DemoModyoJprApplicationTests {
	
	@Autowired
	private PokeModyoAPIController restController;

	@Test
	void contextLoads() {
	}
	
	@Test
	public void testPokemonList() {
		ResponseEntity<List<Pokemon>> response = restController.getPokemons(1); 
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
	}
	
	@Test
	public void testPokemonDetail() {
		//List<Pokemon> response = new ArrayList<>();
		Pokemon response = restController.getPokemon("pikachu"); 
		assertThat(response).isNotEqualTo(null);
	}

}
